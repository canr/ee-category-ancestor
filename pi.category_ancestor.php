<?php if ( !defined ('BASEPATH')) exit ('No direct script access allowed');

/**
 * A plugin to fetch an ancestor category of a given category.
 */


$plugin_info = array (
    'pi_name'       => 'Category Ancestor',
    'pi_version'        => '1.0.0',
    'pi_author'         => 'Eric Slenk',
    'pi_description'    => 'Fetches an ancestor category of a given category.'
);

class Category_ancestor
{

    public function __construct ()
    {
        require realpath(dirname(__FILE__).'/../../../../files/sitewide/Fetch/Fetch.php');

        // Fetch the depth
        $depth = get_instance()->TMPL->fetch_param('depth');
        if ( ! $depth || $depth < 1 || ! is_int($depth) )
            $depth = 1;

        // Fetch one of the current category parameters and fetch the appropriate ID
        try
        {
            if ( $current_category_id = get_instance()->TMPL->fetch_param('cat_id') )
            {
                $this->return_data = $fetch->CategoryIdByDescendentCategoryIdAndDepth( $current_category_id );
            }
            elseif ( $current_category_url_title = get_instance()->TMPL->fetch_param('url_title') )
            {
                $this->return_data = $fetch->CategoryIdByDescendentCategoryUrlTitleAndDepth( $current_category_url_title );
            }
            else
            {
                $this->return_data = '';
            }
        }
        catch (Exception $exception)
        {
            $this->return_data = '';
        }

    }

};
?>